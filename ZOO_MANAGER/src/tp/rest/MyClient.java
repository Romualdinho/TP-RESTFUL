package tp.rest;

import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;

import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

public class MyClient {
    private Service service;
    private JAXBContext jc;

    private static final QName qname = new QName("", "");
    private static final String url = "http://127.0.0.1:8084";

    public MyClient() {
        try {
            jc = JAXBContext.newInstance(Center.class, Cage.class, Animal.class, Position.class);
        } catch (JAXBException je) {
            System.out.println("Cannot create JAXBContext " + je);
        }
    }

    /**
     * Returns all of the animals of the center
     */
    public void get_animals() {
    	printSource(getSource(url + "/animals", "GET", null));
    }
    
    /**
     * Returns the animal identified by id
     */
    public void get_animal_with_id(UUID id) {
    	printSource(getSource(url + "/animals/" + id, "GET", null));
    }
    
    /**
     * Searches for an animal by name
     */
    public void get_animal_with_name(String name) {
    	printSource(getSource(url + "/find/byName/" + name, "GET", null));
    }
    
    /**
     * Searches for an animal by position
     */
    public void get_animal_at_position(String position) {
    	printSource(getSource(url + "/find/at/" + position, "GET", null));
    }
    
    /**
     * Searches animals near a position
     */
    public void get_animals_near_position(String position) {
    	printSource(getSource(url + "/find/near/" + position, "GET", null));
    }
    
    /**
     * Retrieves informations on the animal identified by id with Wolfram service
     */
    public void get_animal_informations(UUID id) {
    	printSource(getSource(url + "/animals/" + id + "/wolf", "GET", null));
    }
    
    /**
     * Retrieves route informations from a GPS location to your center using the GraphHopper service
     */
    public void get_route_informations(String position) {
    	printSource(getSource(url + "/center/journey/from/" + position, "GET", null));
    }
    
    /**
     * Returns the cage named name
     */
    public void get_cage_with_name(String name) {
    	printSource(getSource(url + "/cages/" + name, "GET", null));
    }
    
    /**
     * Adds an animal to your center
     */
    public void add_animal(Animal animal) throws JAXBException {
    	printSource(getSource(url + "/animals", "POST", new JAXBSource(jc, animal)));
    }
    
    /**
     * Adds a cage to your center
     */
    public void add_cage(Cage cage) throws JAXBException {
    	printSource(getSource(url + "/cages", "POST", new JAXBSource(jc, cage)));
    }
    
    /**
     * Creates the animal identified by id
     */
    public void add_animal_with_id(Animal animal, UUID id) throws JAXBException {
        printSource(getSource(url + "/animals/" + id, "POST", new JAXBSource(jc, animal)));
    }
    
    /**
     * Edits the animal identified by id
     */
    public void edit_animal_with_id(Animal animal, UUID id) throws JAXBException {
    	printSource(getSource(url + "/animals/" + id, "PUT", new JAXBSource(jc, animal)));
    }
    
    /**
     * Edits the cage named name
     */
    public void edit_cage_with_name(Cage cage, String name) throws JAXBException {
    	printSource(getSource(url + "/cages/" + name, "PUT", new JAXBSource(jc, cage)));
    }
    
    /**
     * Edits all animals
     */
    public void edit_all_animals(Animal animal) throws JAXBException {
    	printSource(getSource(url + "/animals", "PUT", new JAXBSource(jc, animal)));
    }
    
    /**
     * Edits all cages
     */
    public void edit_all_cages(Cage cage) throws JAXBException {
        printSource(getSource(url + "/cages", "PUT", new JAXBSource(jc, cage)));
    }
    
    /**
     * Deletes the animal identified by id
     */
    public void delete_animal_with_id(UUID id) throws JAXBException {
    	printSource(getSource(url + "/animals/" + id, "DELETE", null));
    }
    
    /**
     * Deletes the animal identified by name
     */
    public void delete_cage_with_name(String name) throws JAXBException {
    	printSource(getSource(url + "/cages/" + name, "DELETE", null));
    }
    
    /**
     * Deletes all animals
     */
    public void delete_all_animals() throws JAXBException {
    	printSource(getSource(url + "/animals", "DELETE", null));
    }
    
    /**
     * Deletes all cages
     */
    public void delete_all_cages() throws JAXBException {
        printSource(getSource(url + "/cages", "DELETE", null));
    }

    public void printSource(Source s) {
        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            transformer.transform(s, new StreamResult(System.out));
        } catch (Exception e) {
            System.out.println(e + "\n");
        }
    }
    
    private Source getSource(String url, String method, Source body) {
    	// Initialisation du service
    	service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url);
        
        // Cr�ation du dispatcher
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, method);

        // Retourne le r�sultat de la requ�te
        return dispatcher.invoke(body);
    }

    public static void main(String args[]) throws Exception {
        MyClient client = new MyClient();
        
        // Sc�nario de test

        System.out.println("<!-- Affichez l'ensemble des animaux -->");
        client.get_animals();
        System.out.println();
        
        System.out.println("<!-- Supprimez tous les animaux -->");
        client.delete_all_animals();
        System.out.println();
        
        System.out.println("<!-- Affichez l'ensemble des animaux -->");
        client.get_animals();
        System.out.println();
        
        System.out.println("<!-- Ajoutez un Panda � Rouen (Latitude : 49.443889 ; Longitude : 1.103333) -->");
        client.add_cage(new Cage("Rouen", new Position(49.443889, 1.103333), 20, new LinkedList<Animal>()));
        client.add_animal(new Animal("Pandi", "Rouen", "Panda", UUID.randomUUID()));
        System.out.println();
        
        System.out.println("<!-- Ajoutez un Hocco unicorne � Paris (Latitude : 48.856578 ; Longitude : 2.351828) -->");
        client.add_cage(new Cage("Paris", new Position(48.856578, 2.351828), 10, new LinkedList<Animal>()));
        client.add_animal(new Animal("Maxine", "Paris", "Hocco unicorne", UUID.randomUUID()));
        System.out.println();
        
        System.out.println("<!-- Affichez l'ensemble des animaux -->");
        client.get_animals();
        System.out.println();
        
        System.out.println("<!-- Modifiez l'ensemble des animaux par un Lagotriche � queue jaune � Rouen (Latitude :49.443889 ; Longitude : 1.103333) -->");
        client.edit_all_animals(new Animal("Janet", "Rouen", "Lagotriche � queue jaune", UUID.randomUUID()));
        System.out.println();
        
        System.out.println("<!-- Affichez l'ensemble des animaux -->");
        client.get_animals();
        System.out.println();
        
        System.out.println("<!-- Ajoutez une Oc�anite de Matsudaira en Somalie (Latitude : 2.333333 ; Longitude : 45.85) -->");
        client.add_cage(new Cage("Somalie", new Position(2.333333, 45.85), 10, new LinkedList<Animal>()));
        client.add_animal(new Animal("Laura", "Somalie", "Oc�anite de Matsudaira", UUID.randomUUID()));
        System.out.println();
        
        System.out.println("<!-- Ajoutez un Tuittuit � Paris (Latitude : 48.856578 ; Longitude : 2.351828) -->");
        client.add_animal(new Animal("Mary", "Paris", "Tuit-tuit", UUID.randomUUID()));
        System.out.println();
        
        System.out.println("<!-- Ajoutez une Sa�ga au Canada (Latitude : 43.2 ; Longitude : -80.38333) -->");
        client.add_cage(new Cage("Canada", new Position(43.2, -80.38333), 10, new LinkedList<Animal>()));
        UUID saigaId = UUID.randomUUID();
        client.add_animal(new Animal("William", "Canada", "Sa�ga", saigaId));
        System.out.println();
        
        System.out.println("<!-- Ajoutez un Galago de Rondo � Bihorel (Latitude : 49.455278 ; Longitude : 1.116944) -->");
        client.add_cage(new Cage("Bihorel", new Position(49.455278, 1.116944), 10, new LinkedList<Animal>()));
        UUID galagoId = UUID.randomUUID();
        client.add_animal(new Animal("Denise", "Bihorel", "Galago de Rondo", galagoId));
        System.out.println();
        
        System.out.println("<!-- Ajoutez un Ara de Spix � Rouen (Latitude : 49.443889 ; Longitude : 1.103333) -->");
        UUID araId = UUID.randomUUID();
        client.add_animal(new Animal("Jack", "Rouen", "Ara de Spix", araId));
        System.out.println();
        
        System.out.println("<!-- Ajoutez une Palette des Sulu � Londres (Latitude : 51.504872 ; Longitude : -0.07857) -->");
        client.add_cage(new Cage("Londres", new Position(51.504872, -0.07857), 10, new LinkedList<Animal>()));
        client.add_animal(new Animal("Vincent", "Londres", "Palette des Sulu", UUID.randomUUID()));
        System.out.println();
        
        System.out.println("<!-- Ajoutez un Kouprey � Paris (Latitude : 48.856578 ; Longitude : 2.351828) -->");
        client.add_animal(new Animal("Christopher", "Paris", "Kouprey", UUID.randomUUID()));
        System.out.println();
        
        System.out.println("<!-- Ajoutez un Inca de Bonaparte � PortoVecchio (Latitude : 41.5895241 ; Longitude : 9.2627) -->");
        client.add_cage(new Cage("PortoVecchio", new Position(41.5895241, 9.2627), 10, new LinkedList<Animal>()));
        client.add_animal(new Animal("John", "PortoVecchio", "Inca de Bonaparte", UUID.randomUUID()));
        System.out.println();
        
        System.out.println("<!-- Affichez l'ensemble des animaux -->");
        client.get_animals();
        System.out.println();
        
        System.out.println("<!-- Ajoutez un Rhinoc�ros de Java � VillersBocage (Latitude : 50.0218 ; Longitude : 2.3261) -->");
        client.add_cage(new Cage("VillersBocage", new Position(50.0218, 2.3261), 10, new LinkedList<Animal>()));
        client.add_animal(new Animal("Edward", "VillersBocage", "Rhinoc�ros de Java", UUID.randomUUID()));
        System.out.println();
        
        System.out.println("<!-- Ajoutez un R�le de Zapata � Montreux (Latitude : 46.4307133; Longitude : 6.9113575) -->");
        client.add_cage(new Cage("Montreux", new Position(46.4307133, 6.9113575), 10, new LinkedList<Animal>()));
        client.add_animal(new Animal("Jeffery", "Montreux", "R�le de Zapata", UUID.randomUUID()));
        System.out.println();
        
        System.out.println("<!-- Ajoutez 101 Dalmatiens dans une cage aux USA -->");
        for (int i = 0; i < 100; i++) {
        	client.add_animal(new Animal("Dalmatien " + (i + 1), "usa", "Dalmatien", UUID.randomUUID()));
        }
        System.out.println();
        
        System.out.println("<!-- Affichez l'ensemble des animaux -->");
        client.get_animals();
        System.out.println();
        
        System.out.println("<!-- Supprimez tous les animaux de Paris -->");
        client.delete_cage_with_name("Paris");
        System.out.println();
        
        System.out.println("<!-- Affichez l'ensemble des animaux -->");
        client.get_animals();
        System.out.println();
        
        System.out.println("<!-- Recherchez l'Ara de Spix -->");
        client.get_animal_with_id(araId);
        System.out.println();
        
        System.out.println("<!-- Supprimez l'Ara de Spix -->");
        client.delete_animal_with_id(araId);
        System.out.println();
        
        System.out.println("<!-- Supprimez � nouveau l'Ara de Spix -->");
        client.delete_animal_with_id(araId);
        System.out.println();
        
        System.out.println("<!-- Affichez l'ensemble des animaux -->");
        client.get_animals();
        System.out.println();
        
        System.out.println("<!-- Affichez les animaux situ�s pr�s de Rouen -->");
        client.get_animals_near_position("lat=49&lng=1.4");
        System.out.println();

        System.out.println("<!-- Affichez un animal � Rouen -->");
        client.get_animal_at_position("lat=49.443889&lng=1.103333");
        System.out.println();
        
        System.out.println("<!-- Affichez les informations Wolfram Alpha du Sa�ga -->");
        client.get_animal_informations(saigaId);
        System.out.println();
        
        System.out.println("<!-- Affichez les informations Wolfram Alpha du Galago de Rondo -->");
        client.get_animal_informations(galagoId);
        System.out.println();
        
        System.out.println("<!-- Affichez le trajet de Londres jusqu'au centre -->");
        client.get_route_informations("lat=51.504872&lng=0.07857");
        System.out.println();
        
        System.out.println("<!-- Supprimez tous les animaux -->");
        client.delete_all_animals();
        System.out.println();
        
        System.out.println("<!-- Affichez l'ensemble des animaux -->");
        client.get_animals();
        System.out.println();
    }
}
