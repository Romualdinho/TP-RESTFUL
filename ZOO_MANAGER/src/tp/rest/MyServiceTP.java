package tp.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Provider;
import javax.xml.ws.Service;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;
import javax.xml.ws.http.HTTPException;

import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;
import tp.model.exceptions.CenterException;
import tp.model.exceptions.RestException;

@WebServiceProvider
@ServiceMode(value = Service.Mode.MESSAGE)
public class MyServiceTP implements Provider<Source> {

    public final static String url = "http://127.0.0.1:8084/";
    
    /**
     * API Keys
     */
    private final static String WOLFRAM_API_KEY = "WX2KQW-YEKXQYTTV4";
    private final static String GRAPHHOPPER_API_KEY = "25bf2c42-b773-4d10-8f32-c48af5a91a9e";
    
    /**
     * Patterns
     */
    private final static Pattern UUID_PATTERN = Pattern.compile("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$");
    private final static Pattern POSITION_PATTERN = Pattern.compile("^lat=\\d*.\\d*&lng=\\d*.\\d*$");
    private final static Pattern ROUTE_PATTERN = Pattern.compile("^lat=\\d*.\\d*&lng=\\d*.\\d*(&vehicle=(car|small_truck|truck|scooter|foot|hike|bike|mtb|racingbike))?$");
    
    public static void main(String args[]) {
        Endpoint e = Endpoint.create(HTTPBinding.HTTP_BINDING, new MyServiceTP());

        e.publish(url);
        System.out.println("Service started, listening on " + url);
        // pour arrêter : e.stop();
    }

    private JAXBContext jc;

    @javax.annotation.Resource(type = Object.class)
    protected WebServiceContext wsContext;

    private Center center = new Center(new LinkedList<>(), new Position(49.30494d, 1.2170602d), "Biotropica");

    public MyServiceTP() {
        try {
            jc = JAXBContext.newInstance(Center.class, Cage.class, Animal.class, Position.class, RestException.class);
        } catch (JAXBException je) {
            System.out.println("Exception " + je);
            throw new WebServiceException("Cannot create JAXBContext", je);
        }

        // Fill our center with some animals
        Cage usa = new Cage(
                "usa",
                new Position(49.305d, 1.2157357d),
                25,
                new LinkedList<Animal>(Arrays.asList(
                        new Animal("Tic", "usa", "Chipmunk", UUID.randomUUID()),
                        new Animal("Tac", "usa", "Chipmunk", UUID.randomUUID())
                ))
        );

        Cage amazon = new Cage(
                "amazon",
                new Position(49.305142d, 1.2154067d),
                15,
                new LinkedList<Animal>(Arrays.asList(
                        new Animal("Canine", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Incisive", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Molaire", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("De lait", "amazon", "Piranha", UUID.randomUUID())
                ))
        );

        center.getCages().addAll(Arrays.asList(usa, amazon));
    }
    
    public Source invoke(Source source) {
        MessageContext mc = wsContext.getMessageContext();
        String path = (String) mc.get(MessageContext.PATH_INFO);
        String method = (String) mc.get(MessageContext.HTTP_REQUEST_METHOD);

        // no target, throw a 404 exception.
        if (path == null) {
            throw new HTTPException(404);
        }
        
        // determine the targeted ressource of the call
        try {
            String[] path_parts = path.split("/");
            
            // "/animals" target - Redirect to the method in charge of managing this sort of call.
            if (path.startsWith("animals")) {
            	if (path_parts.length == 1) {
            		return this.animalsCrud(method, source);
            	} else if (path_parts.length == 2) {
            		return this.animalCrud(method, source, path_parts[1]);
            	} else if (path_parts.length == 3 && path_parts[2].equals("wolf")) {
            		return this.animalWolframCrud(method, source, path_parts[1]);
            	}
            }
            // "/cages" target - Redirect to the method in charge of managing this sort of call. (ADDITIONAL FEATURES)
            else if (path.startsWith("cages")) {
            	if (path_parts.length == 1) {
            		return this.cagesCrud(method, source);
            	} else if (path_parts.length == 2) {
            		return this.cageCrud(method, source, path_parts[1]);
            	}
            }
            // "/find" target - Redirect to the method in charge of managing this sort of call.
            else if (path.startsWith("find") && path_parts.length == 3) {
                return this.findCrud(method, source, path_parts[1], path_parts[2]);
            }
            // "/center/journey/from" target - Redirect to the method in charge of managing this sort of call.
            else if (path.startsWith("center/journey/from") && path_parts.length == 4) {
            	return this.routeCrud(method, source, path_parts[3]);
            }
        } catch (JAXBException e) {
            throw new HTTPException(500);
        }
        
        throw new HTTPException(404);
    }
    
    /**
     * Method bound to calls on /animals/{something}
     */
	private Source animalCrud(String method, Source source, String animal_id) throws JAXBException {
		if (!UUID_PATTERN.matcher(animal_id).matches()) {
			return new JAXBSource(this.jc, new RestException("Invalid UUID format (expected format : " + UUID_PATTERN + ")"));
		}
		
		try {
			switch (method) {
			case "GET":
				return new JAXBSource(this.jc, center.findAnimalById(UUID.fromString(animal_id)));
			case "POST":
				return new JAXBSource(this.jc, center.createAnimalById(unmarshalAnimal(source), UUID.fromString(animal_id)));
			case "PUT":
				return new JAXBSource(this.jc, center.editAnimalById(unmarshalAnimal(source), UUID.fromString(animal_id)));
			case "DELETE":
				return new JAXBSource(this.jc, center.deleteAnimalById(UUID.fromString(animal_id)));
			default:
				throw new HTTPException(404);
			}
		} catch (CenterException e) {
			return new JAXBSource(this.jc, new RestException(e.getMessage()));
		}
	}

    /**
     * Method bound to calls on /animals
     */
    private Source animalsCrud(String method, Source source) throws JAXBException {
    	try {
    		switch (method) {
			case "GET":
				return new JAXBSource(this.jc, center);
			case "POST":
				return new JAXBSource(this.jc, center.addAnimal(unmarshalAnimal(source)));
			case "PUT":
				return new JAXBSource(this.jc, center.editAllAnimals(unmarshalAnimal(source)));
			case "DELETE":
				return new JAXBSource(this.jc, center.deleteAllAnimals());
			default:
				throw new HTTPException(404);
			}
    	} catch (CenterException e) {
    		return new JAXBSource(this.jc, new RestException(e.getMessage()));
    	}
    }
    
    /**
     * Method bound to calls on /animals/{something}/wolf
     */
	private Source animalWolframCrud(String method, Source source, String animal_id) throws JAXBException {
		if (!method.equals("GET")) {
			throw new HTTPException(404);
		}
		
		if (!UUID_PATTERN.matcher(animal_id).matches()) {
			return new JAXBSource(this.jc, new RestException("Invalid UUID format (expected format : " + UUID_PATTERN + ")"));
		}
		
		try {
			// R�cup�ration de l'animal dont on souhaite obtenir des informations
			Animal animal = center.findAnimalById(UUID.fromString(animal_id));
			
			//Forme l'URL en respectant l'API impos�e par WolframAlpha
			String url = String.format("http://api.wolframalpha.com/v2/query?appid=%s&input=%s", WOLFRAM_API_KEY, animal.getSpecies());

	    	try {
	    		return getResultFromService(url, method);
	    	} catch (IOException e) {
	    		throw new HTTPException(404);
	    	}
		} catch (CenterException e) {
			return new JAXBSource(this.jc, new RestException(e.getMessage()));
		}
	}
    
    /**
     * Method bound to calls on /cages/{something}
     */
    private Source cageCrud(String method, Source source, String cage_name) throws JAXBException {
		try {
			switch (method) {
			case "GET":
				return new JAXBSource(this.jc, center.findCageByName(cage_name));
			case "PUT":
				return new JAXBSource(this.jc, center.editCageByName(unmarshalCage(source), cage_name));
			case "DELETE":
				return new JAXBSource(this.jc, center.deleteCageByName(cage_name));
			default:
				throw new HTTPException(404);
			}
		} catch (CenterException e) {
			return new JAXBSource(this.jc, new RestException(e.getMessage()));
		}
    }
    
    /**
     * Method bound to calls on /cages
     */
    private Source cagesCrud(String method, Source source) throws JAXBException {
    	try {
			switch (method) {
			case "POST":
				return new JAXBSource(this.jc, center.addCage(unmarshalCage(source)));
			case "PUT":
				return new JAXBSource(this.jc, center.editAllCages(unmarshalCage(source)));
			case "DELETE":
				return new JAXBSource(this.jc, center.deleteAllCages());
			default:
				throw new HTTPException(404);
			}
    	} catch (CenterException e) {
    		return new JAXBSource(this.jc, new RestException(e.getMessage()));
    	}
    }
    
    /**
     * Method bound to calls on /find/byName/{something}
     * 							/find/at/{something}
     * 							/find/near/{something}
     */
    private Source findCrud(String method, Source source, String searchCriterion, String searchValue) throws JAXBException {
    	if (!method.equals("GET")) {
    		throw new HTTPException(404);
    	}

    	try {
    		switch (searchCriterion) {
			case "byName":
				return new JAXBSource(this.jc, center.findAnimalByName(searchValue));
			case "at":
				if (!POSITION_PATTERN.matcher(searchValue).matches()) return new JAXBSource(this.jc, new RestException("Invalid position format (expected format : " + POSITION_PATTERN + ")"));
				return new JAXBSource(this.jc, center.findAnimalByPosition(parsePosition(searchValue)));
			case "near":
				if (!POSITION_PATTERN.matcher(searchValue).matches()) return new JAXBSource(this.jc, new RestException("Invalid position format (expected format : " + POSITION_PATTERN + ")"));
				return new JAXBSource(this.jc, center.findAnimalsNearPosition(parsePosition(searchValue)));
			default:
				throw new HTTPException(404);
			}
    	} catch (CenterException e) {
    		return new JAXBSource(this.jc, new RestException(e.getMessage()));
    	}
    }
    
    /**
     * Method bound to calls on /center/journey/from/{something}
     */
    private Source routeCrud(String method, Source source, String route) throws JAXBException {
    	if (!method.equals("GET")) {
    		throw new HTTPException(404);
    	}
    	
    	if (!ROUTE_PATTERN.matcher(route).matches()) {
    		return new JAXBSource(this.jc,  new RestException("Invalid route format (expected format : " + ROUTE_PATTERN + ")"));
    	}
    	
    	// V�hicule par d�faut si l'utilisateur n'a rien sp�cifi�
    	String vehicle = "car";
    	
    	// V�rifie si l'utilisateur a sp�cifi� un v�hicule particulier pour l'�laboration du trajet
    	if (route.contains("&vehicle=")) {
    		// R�cup�ration du v�hicule d�sir�
    		vehicle = route.substring(route.indexOf("&vehicle=") + 9);
    	}
    	
    	// R�cup�re la position de d�part du trajet
    	Position sourcePosition = parsePosition(route.substring(0, Math.max(route.indexOf("&vehicle="), route.length())));
    	
    	// La position de destination du trajet correspond � l'adresse du centre
    	Position targetPosition = center.getPosition();
    	
    	// Forme l'URL en respectant l'API impos�e par GraphHopper
    	String url = String.format(Locale.US, "https://graphhopper.com/api/1/route?point=%f,%f&point=%f,%f&vehicle=%s&locale=fr&type=gpx&key=%s", 
    			sourcePosition.getLatitude(), sourcePosition.getLongitude(), targetPosition.getLatitude(), 
    			targetPosition.getLongitude(), vehicle, GRAPHHOPPER_API_KEY);
    	
    	try {
    		return getResultFromService(url, method);
    	} catch (IOException e) {
    		throw new HTTPException(404);
    	}
    }
    
    /**
     * Unmarshal Source to Animal
     */
    private Animal unmarshalAnimal(Source source) throws JAXBException {
        return (Animal) this.jc.createUnmarshaller().unmarshal(source);
    }
    
    /**
     * Unmarshal Source to Cage
     */
    private Cage unmarshalCage(Source source) throws JAXBException {
    	Cage cage = (Cage) this.jc.createUnmarshaller().unmarshal(source);
    	
       	// V�rifie que la liste est instanci�e suite au unmarshal
    	if (cage.getResidents() == null) {
    		cage.setResidents(new LinkedList<Animal>());
    	}
    	
    	return cage;
    }
    
    /**
     * Parse String to Position
     */
    private Position parsePosition(String value)  {
    	String position_parts[] =  value.split("&");
    	return new Position(Double.parseDouble(position_parts[0].replace("lat=", "")), Double.parseDouble(position_parts[1].replaceAll("lng=", "")));
    }
    
    /**
     * Execute a request and return the result
     */
    private Source getResultFromService(String url, String method) throws IOException {
    	// Effectue une requ�te HTTP � l'URL indiqu�
    	HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
    	
    	// Methode de la requ�te
    	connection.setRequestMethod(method);
		
    	// R�cup�ration du r�sultat de la requ�te dans un BufferedReader
    	BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
        
    	// Lecture du r�sultat
    	StringBuilder response = new StringBuilder();
        String currentLine;
        while ((currentLine = br.readLine()) != null) {
            response.append(currentLine);
        }
        
        br.close();
        
        // Retourne le r�sultat
        return new StreamSource(new StringReader(String.valueOf(response)));
    }
}
