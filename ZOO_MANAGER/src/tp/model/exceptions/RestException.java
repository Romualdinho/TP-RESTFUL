package tp.model.exceptions;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RestException {

	private String message;
	
	public RestException() {
	}
	
	public RestException(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String toString() {
		return message;
	}
}
