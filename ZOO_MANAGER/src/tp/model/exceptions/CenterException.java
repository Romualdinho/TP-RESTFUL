package tp.model.exceptions;

public class CenterException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public CenterException(String message) {
		super(message);
	}
	
	public CenterException() {
		this("");
	}
}
