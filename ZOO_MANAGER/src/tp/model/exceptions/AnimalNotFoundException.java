package tp.model.exceptions;

public class AnimalNotFoundException extends CenterException {

	private static final long serialVersionUID = 1L;
	
	public AnimalNotFoundException(String message) {
		super(message);
	}
}
