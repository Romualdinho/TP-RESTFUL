package tp.model;

import javax.xml.bind.annotation.XmlRootElement;

import tp.model.exceptions.AnimalNotFoundException;
import tp.model.exceptions.CageNotFoundException;
import tp.model.exceptions.CenterException;

import java.util.Collection;
import java.util.LinkedList;
import java.util.UUID;

@XmlRootElement
public class Center {

    Collection<Cage> cages;
    Position position;
    String name;

    public Center() {
        cages = new LinkedList<>();
    }

    public Center(Collection<Cage> cages, Position position, String name) {
        this.cages = cages;
        this.position = position;
        this.name = name;
    }
    
    public Cage findCageByName(String name) throws CageNotFoundException {
    	return cages.stream()
    	        .filter(cage -> cage.getName().equalsIgnoreCase(name))
    	        .findFirst()
    	        .orElseThrow(() -> new CageNotFoundException("No cage found with name : " + name));
    }

    public Animal findAnimalById(UUID uuid) throws AnimalNotFoundException {
        return cages.stream()
                .map(Cage::getResidents)
                .flatMap(Collection::stream)
                .filter(animal -> uuid.equals(animal.getId()))
                .findFirst()
                .orElseThrow(() -> new AnimalNotFoundException("No animal found with ID : " + uuid));
    }
    
    public Animal findAnimalByName(String name) throws AnimalNotFoundException {
        return cages.stream()
                .map(Cage::getResidents)
                .flatMap(Collection::stream)
                .filter(animal -> name.equalsIgnoreCase(animal.getName()))
                .findFirst()
                .orElseThrow(() -> new AnimalNotFoundException("No animal found with name : " + name));
    }
    
    public Animal findAnimalByPosition(Position position) throws AnimalNotFoundException {
    	return cages.stream()
    	        .filter(cage -> position.equals(cage.getPosition()))
    	        .map(Cage::getResidents)
    	        .flatMap(Collection::stream)
    	        .findFirst()
    	        .orElseThrow(() -> new AnimalNotFoundException("No animal found at the position : " + position));
    }
    
    public Cage findAnimalsNearPosition(Position position) throws CageNotFoundException {
    	return cages.stream()
    	        .filter(cage -> cage.getPosition().isNear(position))
    	        .findFirst()
    			.orElseThrow(() -> new CageNotFoundException("No animals found near the position : " + position));
    }
    
    public Center addAnimal(Animal animal) throws CenterException {
    	// R�cup�ration de la cage de destination du nouvel animal
    	Cage cage = findCageByName(animal.getCage());
    	
    	// V�rifie que la capacit� de la cage peut accepter un nouvel animal
    	if (cage.getCapacity() <= cage.getResidents().size()) {
    		throw new CenterException("The maximum capacity of the cage " + cage.getName () + " is already reached");
    	}
    	
    	// Ajout de l'animal dans sa cage
    	cage.getResidents().add(animal);
    	
    	return this;
    }
    
    public Center addCage(Cage cage) throws CenterException {
    	try {
    		// V�rifie qu'une cage ayant le m�me nom n'existe pas d�j�
			Cage c = findCageByName(cage.getName());
			throw new CenterException("The cage " + c.getName() + " already exists");
		} catch (CageNotFoundException e) {		
	    	// Ajout de la cage au centre
	    	cages.add(cage);
		}

    	return this;
    }
    
    public Animal createAnimalById(Animal animal, UUID uuid) throws CenterException {
    	// Cr�ation du nouvel animal avec l'UUID pass� en param�tres
    	Animal createdAnimal = new Animal(animal.getName(), animal.getCage(), animal.getSpecies(), uuid);
    	
    	// Ajout de l'animal dans sa cage
    	addAnimal(createdAnimal);
    	
    	return createdAnimal;
    }
    
    public Animal editAnimalById(Animal editedAnimal, UUID uuid) throws AnimalNotFoundException {
    	// R�cup�ration de l'animal � modifier
    	Animal animal = findAnimalById(uuid);
    	
    	// Modifie les caract�ristiques de l'animal par les caract�ristiques de l'animal pass� en param�tres
    	animal.setName(editedAnimal.getName());
    	animal.setCage(editedAnimal.getCage());
    	animal.setSpecies(editedAnimal.getSpecies());
    	
    	return animal;
    }
    
    public Center editAllAnimals(Animal animal) throws CageNotFoundException {
    	// R�cup�ration de la nouvelle cage d�sir�e
    	Cage cage = findCageByName(animal.getCage());

    	Collection<Animal> animals = new LinkedList<Animal>();
    	
    	// R�cup�ration de l'ensemble des animaux
    	cages.forEach(c -> animals.addAll(c.getResidents()));
    	
    	// Vide toutes les cages
    	deleteAllAnimals();
    	
    	// Ajout de l'ensemble des animaux dans la cage d�sir�e
    	cage.setResidents(animals);
    	
    	// Modification des caract�ristiques de l'ensemble des animaux
    	cage.getResidents().forEach(a -> {
    		a.setName(animal.getName());
    		a.setCage(animal.getCage());
    		a.setSpecies(animal.getSpecies());
    	});
        
    	return this;
    }
    
    public Cage editCageByName(Cage editedCage, String cageName) throws CageNotFoundException {
    	// R�cup�ration de la cage � modifier
    	Cage cage = findCageByName(cageName);
    	
    	// Modification des caract�ristiques de la cage par les caract�ristiques de la cage pass�e en param�tres
    	cage.setCapacity(editedCage.getCapacity());
    	cage.setPosition(editedCage.getPosition());
    	cage.setResidents(editedCage.getResidents());
    	
    	return cage;
    }
    
    public Center editAllCages(Cage cage) {
    	// Pour chaque cage
    	cages.forEach(c -> {
    		// Modification des caract�ristiques de la cage par les caract�ristiques de la cage pass�e en param�tres
    		c.setPosition(cage.getPosition());
    		c.setCapacity(cage.getCapacity());
    	});
    	
    	return this;
    }
    
    public Center deleteAnimalById(UUID uuid) throws CenterException {
    	// R�cup�ration de l'animal � supprimer
    	Animal animal = findAnimalById(uuid);
    	
    	// Suppression de l'animal de la cage dans lequel il est
    	findCageByName(animal.getCage()).getResidents().remove(animal);
    	
    	return this;
    }
    
    public Center deleteAllAnimals() {
    	cages.forEach(cage -> cage.getResidents().clear());
    	
    	return this;
    }
    
    public Center deleteCageByName(String name) throws CageNotFoundException {
    	// R�cup�ration de la cage � supprimer
    	Cage cage = findCageByName(name);
    	
    	// Suppression de la cage du centre
    	cages.remove(cage);
    	
    	return this;
    }
    
    public Center deleteAllCages() {
    	cages.clear();
    	
    	return this;
    }

    public Collection<Cage> getCages() {
        return cages;
    }

    public Position getPosition() {
        return position;
    }

    public String getName() {
        return name;
    }

    public void setCages(Collection<Cage> cages) {
        this.cages = cages;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setName(String name) {
        this.name = name;
    }
}
