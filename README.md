# ZOO MANAGER (RESTFUL API)

**Zoo Manager** is an Restful API used to manage a zoo.

## Features

### Expected Features
|**METHOD**|**URL**|**BODY**|**DESCRIPTION**|
| ------------ | ------------ | ------------ | ------------ |
|**GET**|/animals| |Returns all of the animals of the center|
|**POST**|/animals|**Animal**|Adds an animal to your center|
|**GET**|/animals/{animal_id}|   |Returns the animal identified by {animal_id}|
|**PUT**|/animals|**Animal**|Edits all animals|
|**DELETE**|/animals|   |Deletes all animals|
|**POST**|/animals/{animal_id}|**Animal**|Creates the animal identified by {animal_id}|
|**PUT**|/animals/{animal_id}|**Animal**|Edits the animal identified by {animal_id}|
|**DELETE**|/animals/{animal_id}|   |Deletes the animal identified by {animal_id}|
|**GET**|/find/byName/{name}|   |Searches for an animal by name|
|**GET**|/find/at/{position}|   |Searches for an animal by position|
|**GET**|/find/near/{position}|   |Searches animals near a position (within a radius of 100 km)|
|**GET**|/animals/{animal_id}/wolf|   |Retrieves informations on the animal identified by {animal_id} using the [WolframAlpha][1] service|
|**GET**|/center/journey/from/{route}|   |Retrieves route information from a GPS location to your center using the [GraphHopper][2] service|

### Additional Features

|**METHOD**|**URL**|**BODY**|**DESCRIPTION**|
| ------------ | ------------ | ------------ | ------------ |
|**POST**|/cages|**Cage**|Adds a cage to your center|
|**GET**|/cages/{cage_name}|   |Returns the cage named {cage_name}|
|**PUT**|/cages|**Cage**|Edits all cages|
|**DELETE**|/cages|   |Deletes all cages|
|**PUT**|/cages/{cage_name}|**Cage**|Edits the cage named {cage_name}|
|**DELETE**|/cages/{cage_name}|   |Deletes the cage named {cage_name}|

### Attributes Format

- **animal_id** : `^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$`
- **position** : `^lat=\d*.\d*&lng=\d*.\d*$`
- **route** : `^lat=\d*.\d*&lng=\d*.\d*(&vehicle=(car|small_truck|truck|scooter|foot|hike|bike|mtb|racingbike))?$`
- **cage_name** : `^.*$`

### Examples

- **Get all of the animals of the center :**
```
localhost:8084/animals
```

- **Get the animal identified by a specific UUID :**
```
localhost:8084/animals/5796579d-1cb4-4dde-a84e-834df588d5f6
```

- **Search for an animal named Tic :**
```
localhost:8084/find/byName/Tic
```

- **Search for an animal at Rouen :**
```
localhost:8084/find/at/lat=49.443889&lng=1.103333
```

- **Search animals near Rouen (within a radius of 100 km) :**
```
localhost:8084/find/near/lat=49.443889&lng=1.103333
```

- **Get informations about an animal identified by a specific UUID :**
```
localhost:8084/animals/3f1a02a7-e278-4f49-a893-52966ec35870/wolf
```

- **Get route information from Rouen to the center (car by default) :**
```
localhost:8084/center/journey/from/lat=49.443889&lng=1.103333
```

- **Get route information from Rouen to the center using a bike :**
```
localhost:8084/center/journey/from/lat=49.443889&lng=1.103333&vehicle=bike
```

- **Get the cage named Rouen :**
```
localhost:8084/cages/Rouen
```

## Setting up

1. Run **MyServiceTP** to run the server.
1. Run **MyClient** if you want to use a client to interact with the service

**Service is running on 8084 port by default**.

## Built With

- [Java 8](https://www.java.com) &mdash; A computer programming language
- [JAX-WS](https://en.wikipedia.org/wiki/Java_API_for_XML_Web_Services) &mdash; A Java programming language API for creating web services
- [Wolfram|Alpha](https://www.wolframalpha.com/) &mdash; A computational knowledge engine API
- [GraphHopper](https://graphhopper.com) &mdash; A fast Directions API with world wide data from OpenStreetMap and route optimization


## Contributors

The original contributors can be found in [**AUTHORS.MD**](/AUTHORS.md).


[1]: https://products.wolframalpha.com/api/documentation/ "Wolfram|Alpha"
[2]: https://graphhopper.com/api/1/docs/routing/ "GraphHopper"
